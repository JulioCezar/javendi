class Api::OrdersController < ApplicationController
	def index
		@orders = Order.all
		respond_to do |format|
			format.json { render :json => @orders}
		end	
	end	
	def create
    @order = Order.new(order_params)
    respond_to do |format|
      if @order.save	        
        format.json { render action: 'show', status: :created, location: api_orders_path }
      else
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

	def update
    @order = Order.find(params[:id])
    respond_to do |format|
      if @order.update(order_params)	        
        format.json { render action: 'show', status: :created, location: api_orders_path }
      else	       
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @order = Order.find(params[:id])
    respond_to do |format|
      if @order.destroy
        format.json{ render json: {order: "Order Destroy"}}
      else        
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

	def show
		@order = Order.find(params[:id])
		respond_to do |format|
			format.json
		end	
	end
  private 
    def order_params
      params.require(:order).permit(:user_id,:product_id,:date,:payment_type,:total)
    end 
end
