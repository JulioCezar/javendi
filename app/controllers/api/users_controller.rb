class Api::UsersController < ApplicationController 
  def index 
    @users = User.all
    respond_to do |format| 
      format.json { render :json => @users }
    end
  end


  def create
    @user = User.new(user_params)
    respond_to do |format|
      if @user.save
        format.json { render action: 'show', status: :created, location: api_users_path }
      else
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      @user = User.find(params[:id])
      if @user.update(user_params)        
        format.json { render action: 'show', status: :updated, location: api_users_path }
      else        
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    respond_to do |format|
      @user = User.find(params[:id])
      if @user.destroy
        format.json{render json: {user: "destroyed"}}
      else        
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
    @user = User.find(params[:id])
    respond_to do |format|
      format.json 
    end 
  end

  private
    def user_params
      params.require(:user).permit(:name, :last_name, :email, :phone, :created_at, :updated_at)
    end

end
