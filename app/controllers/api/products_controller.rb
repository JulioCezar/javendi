class Api::ProductsController < ApplicationController
	def index
    @products = Product.all
    respond_to do |format|      
      format.json {render :json => @products}
    end
  end

  def create
    @product = Product.new(product_params)
    respond_to do |format|
      if @product.save        
        format.json { render action: 'show', status: :created, location: api_products_path }
      else
        
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      @product = Product.find(params[:id])
      if @product.update(product_params)        
        format.json { render action: 'show', status: :created, location: api_products_path }
      else
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    respond_to do |format|
      @product = Product.find(params[:id])
      if @product.destroy
        format.json{render json: {product: "destroyed"}}
      else        
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
  	@product = Product.find(params[:id])
  	respond_to do |format|  		
  		format.json
  	end
  end
  private 
    def product_params
      params.require(:product).permit(:name,:description,:photos_file_name,:photos_content_type,:photos_file_size,:photos_updated_at,:category_id)
    end	
end
