class Api::CategoriesController < ApplicationController
  def index
    @categories = Category.all
    respond_to do |format|
      format.json
    end
  end

  def create
    @category = Category.new(category_params)
    respond_to do |format|
      if @category.save
        format.json { render action: 'show', status: :created, location: api_categories_path }
      else
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

	def update
    @category = Category.find(params[:id])
    respond_to do |format|
      if @category.update(category_params)        
        format.json { render action: 'show', status: :created, location: api_categories_path }
      else
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @category = Category.find(params[:id])
    respond_to do |format|
      if @category.destroy
        format.json {render json: {category: "Category Destroy"}}
      else
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

	def show
		@category = Category.find(params[:id])
		respond_to do |format|
			format.json
		end	
	end

  private 
    def category_params
      params.require(:category).permit(:name)
    end


end
